Data Science Assignment;

Code used to clean data etc. can be found in this [file](https://gitlab.com/franklyn07/datascienceassignment/blob/master/201819_CPS3235_SCIBERRAS_FRANKLYN_441498M_assignment_code.zip).
Documentation can be found in this [file](https://gitlab.com/franklyn07/datascienceassignment/blob/master/201819_CPS3235_SCIBERRAS_FRANKLYN_441498M_assignment_doc.pdf.zip).